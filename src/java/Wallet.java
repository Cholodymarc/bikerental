public class Wallet {
    private double cashAmount;

    public Wallet(double cashAmount) {
        this.cashAmount = cashAmount;
    }

    public double getCashAmount() {
        return cashAmount;
    }

    public void addCashAmount(double amount) {
        this.cashAmount += amount;
    }

    public void deductCashAmount(double amount) {
        this.cashAmount -= amount;
        System.out.println(amount + " deducted from your wallet");
    }

    @Override
    public String toString() {
        return " you have "
                + cashAmount
                + " in your wallet";
    }
}
