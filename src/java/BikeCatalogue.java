import java.util.ArrayList;
import java.util.List;

public class BikeCatalogue {
    private List<Bike> bikeList;

    public BikeCatalogue() {
        bikeList = new ArrayList<>();
    }

    public boolean addBike(Bike bike) {
        if (!bikeList.contains(bike)) {
            bikeList.add(bike);
            return true;
        } else {
            return false;
        }
    }

    public void printCatalogue() {
        System.out.println("Bikes in catalogue: ");
        for (int i = 0; i < bikeList.size(); i++) {
            System.out.println(i + " -> " + bikeList.get(i).toString());
        }
    }

    public void printCatalogueByColour(String colour) {
        System.out.println("Available " + colour + " bikes: ");
        for (int i = 0; i < bikeList.size(); i++) {
            if (bikeList.get(i).getColour().equalsIgnoreCase(colour)) {
                System.out.println(i + " -> " + bikeList.get(i).toString());
            }
        }
    }

    public void printCatalogueByBrand(String brand) {
        System.out.println("Available " + brand + " bikes: ");
        for (int i = 0; i < bikeList.size(); i++) {
            if (bikeList.get(i).getBrand().equalsIgnoreCase(brand)) {
                System.out.println(i + " -> " + bikeList.get(i).toString());
            }
        }
    }

    public void printCatalogueByStatus(boolean blnIsBooked) {
        if(blnIsBooked){
            System.out.println("BOOKED bikes: ");
            for (int i = 0; i < bikeList.size(); i++) {
                if (bikeList.get(i).isBooked()) {
                    System.out.println(i + " -> " + bikeList.get(i).toString());
                }
            }
        } else {
            System.out.println("FREE bikes: ");
            for (int i = 0; i < bikeList.size(); i++) {
                if (bikeList.get(i).isBooked() == false) {
                    System.out.println(i + " -> " + bikeList.get(i).toString());
                }
            }
        }
    }

    public void printCatalogueByPrice(double price) {
        System.out.println("Available bikes cheaper than " + price + ":");
        for (int i = 0; i < bikeList.size(); i++) {
            if (bikeList.get(i).getAmountPerHour() <= price) {
                System.out.println(i + " -> " + bikeList.get(i).toString());
            }
        }
    }

    public List<Bike> getBikeList() {
        return bikeList;
    }

    public Bike getBike(int number) {
        try {
            return bikeList.get(number);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("There is no bike " + number + " on the list of bikes");
            return null;
        }
    }

    public boolean rentBike(Client client, int bikeNumber) {
        Bike bikeToRent = this.getBike(bikeNumber);
        if (bikeToRent != null && bikeToRent.rent(client)) {
            return true;
        }
        return false;
    }

    public boolean finishRenting(Client client, int bikeNumber) {
        Bike bikeToHandBack = this.getBike(bikeNumber);
        if (bikeToHandBack != null && bikeToHandBack.finishRenting()) {
            client.getWallet().deductCashAmount(bikeToHandBack.calculateRentalPrice());
            return true;
        }
        return false;
    }
}
