public class Client {
    private String name;
    private Wallet wallet;

    public Client(String name, double cashAmount) {
        this.name = name;
        this.wallet = new Wallet(cashAmount);
    }

    public String getName() {
        return name;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void printRentedBikes(BikeCatalogue bikeCatalogue) {
        System.out.println("Rented bikes: ");
        for (int i = 0; i < bikeCatalogue.getBikeList().size(); i++) {
            if (bikeCatalogue.getBikeList().get(i).getRentedBy().equals(this))
                System.out.println(i + " -> " + bikeCatalogue.getBikeList().get(i).toString());
        }
    }

    @Override
    public String toString() {
//        return name + " you have " + wallet.getCashAmount() + " in your wallet";
        return name + wallet.toString();
    }
}
