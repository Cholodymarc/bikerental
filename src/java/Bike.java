import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class Bike {
    private String brand;
    private String colour;
    private boolean booked;
    private double amountPerHour;
    private RentalPeriod rentalPeriod;
    private Client rentedBy;

    public Bike(String brand, String colour, boolean booked, double amountPerHour) {
        this.brand = brand;
        this.colour = colour;
        this.booked = booked;
        this.amountPerHour = amountPerHour;
        this.rentedBy = new Client("", 0);
        this.rentalPeriod = new RentalPeriod();
    }

    public String getBrand() {
        return brand;
    }

    public String getColour() {
        return colour;
    }

    public boolean isBooked() {
        return booked;
    }

    public double getAmountPerHour() {
        return amountPerHour;
    }

    public Client getRentedBy() {
        return rentedBy;
    }

    public boolean rent(Client client) {
        if (booked) {
            return false;
        }
        rentalPeriod.setStartRent(LocalDateTime.now());
        this.booked = true;
        this.rentedBy = client;
        return true;
    }

    public boolean finishRenting() {
        if (booked == false) {
            return false;
        }
        rentalPeriod.setEndRent(LocalDateTime.now());
        this.booked = false;
        this.rentedBy = new Client("", 0);
        return true;
    }

    public double calculateRentalPrice() {
        long seconds = rentalPeriod.calculateRentalPeriod();
        return (seconds - seconds % 10) * amountPerHour / 10;
    }

    @Override
    public String toString() {
        return "Brand: " + brand
                + " -> "
                + colour
                + " -> "
                + (booked ? "BOOKED" : "FREE")
                + " -> "
                + "amountPerHour = "
                + amountPerHour
                + " -> "
                + "rented by = "
                + rentedBy.getName()
                ;
    }

    public static class RentalPeriod {
        LocalDateTime startRent;
        LocalDateTime endRent;

        private void setStartRent(LocalDateTime startRent) {
            this.startRent = startRent;
        }

        private void setEndRent(LocalDateTime endRent) {
            this.endRent = endRent;
        }

        private long calculateRentalPeriod(){
            return ChronoUnit.SECONDS.between(startRent, endRent);
        }
    }
}
