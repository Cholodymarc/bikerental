import java.util.InputMismatchException;
import java.util.Scanner;

public class RentalRunner {
    private Scanner scanner = new Scanner(System.in);

    private void runApplication() {

        Bike bike1 = new Bike("Pinarello", "RED", false, 10);
        Bike bike2 = new Bike("Eddy Merckx", "BLUE", false, 7);
        Bike bike3 = new Bike("BMC", "BLACK", false, 4);
        Bike bike4 = new Bike("Trek", "GREEN", false, 12);
        Bike bike5 = new Bike("Specialized", "BROWN", false, 15);
        Bike bike6 = new Bike("Giant", "VIOLET", false, 17);
        Bike bike7 = new Bike("Raleigh", "YELLOW", false, 8);
        Bike bike8 = new Bike("GT", "PINK", false, 5);
        Bike bike9 = new Bike("Focus", "GREY", false, 14);
        Bike bike10 = new Bike("Salsa", "ORANGE", false, 15);

        BikeCatalogue bikeCatalogue = new BikeCatalogue();
        bikeCatalogue.addBike(bike1);
        bikeCatalogue.addBike(bike2);
        bikeCatalogue.addBike(bike3);
        bikeCatalogue.addBike(bike4);
        bikeCatalogue.addBike(bike5);
        bikeCatalogue.addBike(bike6);
        bikeCatalogue.addBike(bike7);
        bikeCatalogue.addBike(bike8);
        bikeCatalogue.addBike(bike9);
        bikeCatalogue.addBike(bike10);

        System.out.println("Hello\n" +
                "What's your name?");
        String name = scanner.nextLine();

        System.out.println(name + ", how much money do you have?");
        double cashAmount = scanner.nextDouble();

        Client client = new Client(name, cashAmount);
        System.out.println(client);


        boolean runApplicationFlag = true;
        while (runApplicationFlag) {
            printMenu();

            int option = scanner.nextInt();
            scanner.nextLine();

            switch (option) {
                case 1:
                    bikeCatalogue.printCatalogue();
                    System.out.println("1. Book a bike\n"
                            + "2. Filter by");
                    option = scanner.nextInt();
                    scanner.nextLine();
                    switch (option) {
                        case 1:
                            rentBike(client, bikeCatalogue);
                            break;
                        case 2:
                            printFilterByMenu();
                            filterBy(bikeCatalogue);
                            rentBike(client, bikeCatalogue);
                            break;
                    }
                    break;
                case 2:
                    client.printRentedBikes(bikeCatalogue);
                    finishRenting(client, bikeCatalogue);
                    client.printRentedBikes(bikeCatalogue);
                    break;
                case 3:
                    manageWallet(client);
                    break;
                case 4:
                    if (client.getWallet().getCashAmount() >= 0) {
                        runApplicationFlag = false;
                    } else {
                        printNegativeAmountMessage();
                    }
                    break;
            }
        }
    }

    private void manageWallet(Client client) {
        System.out.println(client);
        System.out.println("Would you like to add cash to your wallet? (Y/N)");
        String reply = scanner.nextLine();
        if ("Y".equalsIgnoreCase(reply)) {
            System.out.println("How much?");
            double amount = scanner.nextDouble();
            client.getWallet().addCashAmount(amount);
            System.out.println(client);
        }
    }

    private void filterBy(BikeCatalogue bikeCatalogue) {
        int option = scanner.nextInt();
        scanner.nextLine();
        switch (option) {
            case 1:
                System.out.println("Filter by colour - enter colour:");
                String colour = scanner.nextLine();
                bikeCatalogue.printCatalogueByColour(colour);
                break;
            case 2:
                System.out.println("Filter by brand - enter brand:");
                String brand = scanner.nextLine();
                bikeCatalogue.printCatalogueByBrand(brand);
                break;
            case 3:
                System.out.println("Filter by status - enter status: (FREE/BOOKED)");
                boolean blnIsBooked = true;
                String status = scanner.nextLine();
                if (status.equalsIgnoreCase("FREE")) {
                    blnIsBooked = false;
                } else if (status.equalsIgnoreCase("BOOKED")) {
                    blnIsBooked = true;
                }
                bikeCatalogue.printCatalogueByStatus(blnIsBooked);
                break;
            case 4:
                System.out.println("Filter by price - enter price:");
                double price = scanner.nextDouble();
                bikeCatalogue.printCatalogueByPrice(price);
                break;
        }
    }

    private boolean rentBike(Client client, BikeCatalogue bikeCatalogue) {
        System.out.println("Which bike would you like to rent?");
        int number = scanner.nextInt();
        if (bikeCatalogue.rentBike(client, number)) {
            client.printRentedBikes(bikeCatalogue);
            return true;
        } else {
            System.out.println("Cannot rent this bike. Bike already rented or not on the list!!!");
            return false;
        }
    }

    private boolean finishRenting(Client client, BikeCatalogue bikeCatalogue) {
        System.out.println("Which bike would you like to return? " +
                "If none press x");
        int number;
        try {
            number = scanner.nextInt();
        } catch (InputMismatchException e) {
            number = -1;
            scanner.nextLine();
        }
        if (bikeCatalogue.finishRenting(client, number)) {
            return true;
        }
        return false;
    }

    private void printMenu() {
        System.out.println("Possible options: (choose number) \n"
                + "1. Show bikes catalogue\n"
                + "2. My booked bikes\n"
                + "3. My Wallet \n"
                + "4. End Application"
        );
    }

    private void printFilterByMenu() {
        System.out.println("You can filter by\n" +
                "1. colour\n" +
                "2. brand\n" +
                "3. rental status\n" +
                "4. price per hour"
        );
    }

    private void printNegativeAmountMessage() {
        System.out.println("Cannot finish application run. " +
                "You have negative cash amount in your wallet." +
                "Please add cash amount to your wallet.");
    }

    public static void main(String[] args) {
        RentalRunner rentalRunner = new RentalRunner();
        rentalRunner.runApplication();
    }
}
